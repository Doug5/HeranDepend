/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicioherandepend;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import model.Aluno;
import model.FuncAdm;
import model.Pessoa;
import model.Professor;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    TextField tfNome;
    @FXML
    TextField tfIdade;
    @FXML
    TextField tfEndereco;
    @FXML
    TextField tfSemestre;
    @FXML
    TextField tfCurso;
    @FXML
    TextField tfDisciplina;
    @FXML
    TextField tfSetor;
    @FXML
    TextField tfFuncao;
    @FXML
    TextField tfProfSalario;
    @FXML
    TextField tfFuncSalario;
    @FXML
    TextField tfNomea;
    @FXML
    TextField tfNomep;
    @FXML
    TextField tfNomefa;
    @FXML
    TextField tfIdadea;
    @FXML
    TextField tfIdadep;
    @FXML
    TextField tfIdadefa;
    @FXML
    TextField tfEnderecoa;
    @FXML
    TextField tfEnderecop;
    @FXML
    TextField tfEnderecofa;
    @FXML
    TextField tfSemestrea;
    @FXML
    TextField tfCursoa;
    @FXML
    TextField tfDisciplinap;
    @FXML
    TextField tfSalariop;
    @FXML
    TextField tfFuncaofa;
    @FXML
    TextField tfSalariofa;
    @FXML
    TextField tfSetorfa;
    @FXML
    RadioButton rbAluno;
    @FXML
    RadioButton rbProfessor;
    @FXML
    RadioButton rbFuncAdm;
    @FXML
    Button cadastrar;
    @FXML
    Button fecharA;
    @FXML
    Button fecharP;
    @FXML
    Button fecharFa;
    @FXML
    Button deletar;
    @FXML
    Button editar;
    @FXML
    Button salvar;
    @FXML
    AnchorPane apAluno;
    @FXML
    AnchorPane apProfessor;
    @FXML
    AnchorPane apFuncAdm;
    @FXML
    AnchorPane detA;
    @FXML
    AnchorPane detP;
    @FXML
    AnchorPane detFa;
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nomeCol;
    @FXML
    private TableColumn<Pessoa, Integer> idadeCol;
    @FXML
    private TableColumn<Pessoa, String> enderecoCol;
    @FXML
    private TableColumn<Pessoa, String> tipoCol;
    @FXML
    private TableColumn<Pessoa, Integer> idCol;
    @FXML
    Button detalhes;
    private ObservableList<Pessoa> pessoas;
    private Pessoa aux;
    
    public void selecAluno(){
        apAluno.setVisible(true);
        apProfessor.setVisible(false);
        apFuncAdm.setVisible(false);
    }
    public void selecProfessor(){
        apAluno.setVisible(false);
        apProfessor.setVisible(true);
        apFuncAdm.setVisible(false);
    }
    public void selecFuncAdm(){
        apAluno.setVisible(false);
        apProfessor.setVisible(false);
        apFuncAdm.setVisible(true);
    }
    
    public void cadastrar(){
        if(rbAluno.isSelected()){
            Aluno a = new Aluno();
            a.setNome(tfNome.getText());
            a.setIdade(Integer.parseInt(tfIdade.getText()));
            a.setEndereco(tfEndereco.getText());
            a.setSemestre(tfSemestre.getText());
            a.setCurso(tfCurso.getText());
            a.setTipo("aluno");
            a.inserir();
            //pessoas.add(a);
            tfSemestre.clear();
            tfCurso.clear();
        }
        if(rbProfessor.isSelected()){
            Professor p = new Professor();
            p.setNome(tfNome.getText());
            p.setIdade(Integer.parseInt(tfIdade.getText()));
            p.setEndereco(tfEndereco.getText());
            p.setDisciplina(tfDisciplina.getText());
            p.setSalario(Double.parseDouble(tfProfSalario.getText()));
            p.setTipo("professor");
            p.inserir();
            //pessoas.add(p);
            tfDisciplina.clear();
            tfProfSalario.clear();
        }
        if(rbFuncAdm.isSelected()){
            FuncAdm fa = new FuncAdm();
            fa.setNome(tfNome.getText());
            fa.setIdade(Integer.parseInt(tfIdade.getText()));
            fa.setEndereco(tfEndereco.getText());
            fa.setSetor(tfSetor.getText());
            fa.setFuncao(tfFuncao.getText());
            fa.setSalario(Double.parseDouble(tfFuncSalario.getText()));
            fa.setTipo("FuncAdm");
            fa.inserir();
            //pessoas.add(fa);
            tfSetor.clear();
            tfFuncao.clear();
            tfFuncSalario.clear();
        }
        atualizaTabela();
        tfNome.clear();
        tfIdade.clear();
        tfEndereco.clear();
    }
    
    public void apareceDetalhes(){
        detalhes.setDisable(false);
        editar.setDisable(false);
        deletar.setDisable(false);
    }
    
    public void detalhes(ActionEvent Event){
        aux = tabela.getSelectionModel().getSelectedItem();
        if(aux.getTipo().equals("aluno")){
            Aluno a = (Aluno) aux;
            tfNomea.setText(a.getNome());
            tfIdadea.setText(String.valueOf(a.getIdade()));
            tfEnderecoa.setText(a.getEndereco());
            tfCursoa.setText(a.getCurso());
            tfSemestrea.setText(a.getSemestre());
            detA.setVisible(true);
        }
        if(aux.getTipo().equals("professor")){
            Professor prof = (Professor) aux;
            tfNomep.setText(prof.getNome());
            tfIdadep.setText(String.valueOf(prof.getIdade()));
            tfEnderecop.setText(prof.getEndereco());
            tfDisciplinap.setText(prof.getDisciplina());
            tfSalariop.setText(String.valueOf(prof.getSalario()));
            detP.setVisible(true);
        }
        if(aux.getTipo().equals("FuncAdm")){
            FuncAdm fa = (FuncAdm) aux;
            tfNomefa.setText(fa.getNome());
            tfIdadefa.setText(String.valueOf(fa.getIdade()));
            tfEnderecofa.setText(fa.getEndereco());
            tfFuncaofa.setText(fa.getFuncao());
            tfSalariofa.setText(String.valueOf(fa.getSalario()));
            tfSetorfa.setText(fa.getSetor());
            detFa.setVisible(true);
        }
    }
    
    
    
    public void clicouEditar(ActionEvent Event){
        editar.setDisable(true);
        cadastrar.setDisable(true);
        deletar.setDisable(true);
        salvar.setDisable(false);
        detalhes.setDisable(true);
        aux = tabela.getSelectionModel().getSelectedItem();
        if(aux.getTipo().equals("aluno")){
            rbProfessor.setDisable(true);
            rbFuncAdm.setDisable(true);
            Aluno a = (Aluno) aux;
            selecAluno();
            rbAluno.setSelected(true);
            tfNome.setText(a.getNome());
            tfIdade.setText(String.valueOf(a.getIdade()));
            tfEndereco.setText(a.getEndereco());
            tfCurso.setText(a.getCurso());
            tfSemestre.setText(a.getSemestre());
            
            //tabela.getItems().remove(a);
        }
        if(aux.getTipo().equals("professor")){
            rbAluno.setDisable(true);
            rbFuncAdm.setDisable(true);
            Professor prof = (Professor) aux;
            selecProfessor();
            rbProfessor.setSelected(true);
            tfNome.setText(prof.getNome());
            tfIdade.setText(String.valueOf(prof.getIdade()));
            tfEndereco.setText(prof.getEndereco());
            tfDisciplina.setText(prof.getDisciplina());
            tfProfSalario.setText(String.valueOf(prof.getSalario()));
            //tabela.getItems().remove(prof);
        }
        if(aux.getTipo().equals("FuncAdm")){
            rbProfessor.setDisable(true);
            rbAluno.setDisable(true);
            FuncAdm fa = (FuncAdm) aux;
            selecFuncAdm();
            rbFuncAdm.setSelected(true);
            tfNome.setText(fa.getNome());
            tfIdade.setText(String.valueOf(fa.getIdade()));
            tfEndereco.setText(fa.getEndereco());
            tfFuncao.setText(fa.getFuncao());
            tfFuncSalario.setText(String.valueOf(fa.getSalario()));
            tfSetor.setText(fa.getSetor());
            //tabela.getItems().remove(fa);
        }
    }
    
    public void clicouDeletar(){
        aux = tabela.getSelectionModel().getSelectedItem();
        aux.delete();
        atualizaTabela();
        editar.setDisable(true);
        salvar.setDisable(true);
        detalhes.setDisable(true);
        deletar.setDisable(true);
    }
    
    public void clicouSalvar(){
        salvar.setDisable(true);
        cadastrar.setDisable(false);
        rbProfessor.setDisable(false);
        rbAluno.setDisable(false);
        rbFuncAdm.setDisable(false);
        salvar();
        //cola
        
    }
    
    public void salvar(){
        if(rbAluno.isSelected()){
            Aluno a = new Aluno();
            a.setId(tabela.getSelectionModel().getSelectedItem().getId());
            a.setNome(tfNome.getText());
            a.setIdade(Integer.parseInt(tfIdade.getText()));
            a.setEndereco(tfEndereco.getText());
            a.setSemestre(tfSemestre.getText());
            a.setCurso(tfCurso.getText());
            a.update();
            tfSemestre.clear();
            tfCurso.clear();
        }
        if(rbProfessor.isSelected()){
            Professor p = new Professor();
            p.setId(tabela.getSelectionModel().getSelectedItem().getId());
            p.setNome(tfNome.getText());
            p.setIdade(Integer.parseInt(tfIdade.getText()));
            p.setEndereco(tfEndereco.getText());
            p.setDisciplina(tfDisciplina.getText());
            p.setSalario(Double.parseDouble(tfProfSalario.getText()));
            p.update();
            tfDisciplina.clear();
            tfProfSalario.clear();
        }
        if(rbFuncAdm.isSelected()){
            FuncAdm fa = new FuncAdm();
            fa.setId(tabela.getSelectionModel().getSelectedItem().getId());
            fa.setNome(tfNome.getText());
            fa.setIdade(Integer.parseInt(tfIdade.getText()));
            fa.setEndereco(tfEndereco.getText());
            fa.setSetor(tfSetor.getText());
            fa.setFuncao(tfFuncao.getText());
            fa.setSalario(Double.parseDouble(tfFuncSalario.getText()));
            fa.update();
            tfSetor.clear();
            tfFuncao.clear();
            tfFuncSalario.clear();
        }
        atualizaTabela();
        tfNome.clear();
        tfIdade.clear();
        tfEndereco.clear();
        
    }
    
    public void atualizaTabela() {
        pessoas = tabela.getItems(); 
        pessoas.clear();        
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));      
        idadeCol.setCellValueFactory(new PropertyValueFactory<>("idade")); 
        enderecoCol.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        tipoCol.setCellValueFactory(new PropertyValueFactory<>("tipo"));
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        
        ArrayList<Pessoa> pessoasBanco = Pessoa.getAll();
        for(Pessoa p: pessoasBanco){
            pessoas.add(p);           
        }

    }  

    public void fecharAluno(){
        detA.setVisible(false);
    }
    
    public void fecharProfessor(){
        detP.setVisible(false);
    }   
    
    public void fecharFuncAdm(){
        detFa.setVisible(false);
    }    
    /*
    public void inicializaPessoas(){
        Aluno a1 = new Aluno();
        a1.setNome("Douglas");
        a1.setIdade(17);
        a1.setEndereco("Niteroi");
        a1.setSemestre("5");
        a1.setCurso("ds3");       
        a1.setTipo("aluno");
        Aluno a2 = new Aluno();
        a2.setNome("Dezordi");
        a2.setIdade(16);
        a2.setEndereco("Guajuviras");
        a2.setSemestre("5");
        a2.setCurso("ds3");
        a2.setTipo("aluno");
        Aluno a3 = new Aluno();
        a3.setNome("Lara");
        a3.setIdade(16);
        a3.setEndereco("Nossa S.");
        a3.setSemestre("3");
        a3.setCurso("ds2");
        a3.setTipo("aluno");
        
        Professor p1 = new Professor();
        p1.setNome("Anderson");
        p1.setIdade(25);
        p1.setEndereco("Mathias");
        p1.setDisciplina("Matematica");
        p1.setSalario(1200);
        p1.setTipo("professor");
        Professor p2 = new Professor();
        p2.setNome("Roberta");
        p2.setIdade(20);
        p2.setEndereco("Rio Branco");
        p2.setDisciplina("Física");
        p2.setSalario(1500);
        p2.setTipo("professor");
        Professor p3 = new Professor();
        p3.setNome("Amanda");
        p3.setIdade(30);
        p3.setEndereco("Centro");
        p3.setDisciplina("Banco de Dados");
        p3.setSalario(1600);
        p3.setTipo("professor");
        
        FuncAdm fa1 = new FuncAdm();
        fa1.setNome("Rogério");
        fa1.setIdade(29);
        fa1.setEndereco("Centro");
        fa1.setFuncao("Secretario");
        fa1.setSetor("Diretoria");
        fa1.setSalario(2000);
        fa1.setTipo("FuncAdm");
        FuncAdm fa2 = new FuncAdm();
        fa2.setNome("Carla");
        fa2.setIdade(45);
        fa2.setEndereco("Estancia Velha");
        fa2.setFuncao("Diretora");
        fa2.setSetor("Diretoria");
        fa2.setSalario(4000);
        fa2.setTipo("FuncAdm");
        FuncAdm fa3 = new FuncAdm();
        fa3.setNome("Camila");
        fa3.setIdade(50);
        fa3.setEndereco("igara");
        fa3.setFuncao("Vice Diretora");
        fa3.setSetor("Diretoria");
        fa3.setSalario(3000);
        fa3.setTipo("FuncAdm");
        
        pessoas.add(a1);
        pessoas.add(a2);
        pessoas.add(a3);
        pessoas.add(p1);
        pessoas.add(p2);
        pessoas.add(p3);
        pessoas.add(fa1);
        pessoas.add(fa2);
        pessoas.add(fa3);
        
        a1.inserir();
        a2.inserir();
        a3.inserir();
        p1.inserir();
        p2.inserir();
        p3.inserir();
        fa1.inserir();
        fa2.inserir();
        fa3.inserir();
    }
    */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizaTabela();
        //inicializaPessoas();
    }    
    
}
