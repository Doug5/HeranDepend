/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 *
 * @author Aluno
 */
public class Pessoa {
    private String nome;
    private int idade;
    private String endereco;
    private String tipo;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public boolean delete(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        
        String insertTableSQL = "DELETE FROM PessoaOO3 WHERE id = ?";
        
        try{
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.id);           
            ps.executeUpdate();
            String insertTableSQL2 = null;
            
            if(this.getTipo().equals("aluno")){
                insertTableSQL2 = "DELETE FROM aluno_oo WHERE id = ?";
            }else if(this.getTipo().equals("professor")){
                insertTableSQL2 = "DELETE FROM professor_oo WHERE id = ?";
            }else if(this.getTipo().equals("FuncAdm")){
                insertTableSQL2 = "DELETE FROM funcadm_oo WHERE id = ?";
            }
            ps2 = dbConnection.prepareStatement(insertTableSQL2);
            ps2.setInt(1, this.id);           
            ps2.executeUpdate();
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return true;
    }
    
    public static ArrayList<Pessoa> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM PessoaOO3";
        Statement st;
        PreparedStatement preparedStatement = null;
        ResultSet rs2;
        
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Pessoa p = null;
                if(rs.getString("tipo").equals("aluno")){
                   String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, aluno_oo.curso, aluno_oo.semestre"
                           + " FROM PessoaOO3"
                           + " INNER JOIN aluno_oo on pessoaoo3.id = aluno_oo.id"
                           + " where aluno_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                   rs2.next();
                   Aluno a = new Aluno();
                   a.setCurso(rs2.getString("curso"));
                   a.setSemestre(rs2.getString("semestre"));
                   p = a;
                }if(rs.getString("tipo").equals("professor")){
                    String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, professor_oo.disciplina, professor_oo.salario"
                           + " FROM PessoaOO3"
                           + " INNER JOIN professor_oo on pessoaoo3.id = professor_oo.id"
                           + " where professor_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                   rs2.next();
                    
                    Professor prof = new Professor();
                    prof.setDisciplina(rs2.getString("disciplina"));
                    prof.setSalario(rs2.getDouble("salario"));
                    p = prof;
                    
                }if(rs.getString("tipo").equals("FuncAdm")){
                    String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, funcadm_oo.funcao, funcadm_oo.setor, funcadm_oo.salario"
                           + " FROM PessoaOO3"
                           + " INNER JOIN funcadm_oo on pessoaoo3.id = funcadm_oo.id"
                           + " where funcadm_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                   rs2.next();
                    
                    FuncAdm fa = new FuncAdm();
                    fa.setFuncao(rs2.getString("funcao"));
                    fa.setSetor(rs2.getString("setor"));
                    fa.setSalario(rs2.getDouble("salario"));
                    p = fa;
                    
                }
          
                p.setId(rs.getInt("id"));
                p.setNome(rs.getString("nome"));
                p.setIdade(rs.getInt("idade"));
                p.setEndereco(rs.getString("endereco"));
                p.setTipo(rs.getString("tipo"));
                
                lista.add(p);
                
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }

    /*
    public static ArrayList<Pessoa> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<Pessoa> lista = new ArrayList<>();
        String selectSQL = "SELECT * FROM PessoaOO";
        Statement st;
        PreparedStatement preparedStatement = null;
        ResultSet rs2;
        
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                Pessoa p = null;
                if(rs.getString("tipo").equals("aluno")){
                   String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, aluno_oo.curso, aluno_oo.semestre"
                           + " FROM PessoaOO"
                           + "INNER JOIN aluno_oo on pessoaoo3.id = aluno_oo.id"
                           + "where aluno_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                   
                   Aluno a = new Aluno();
                   a.setCurso(rs2.getString("curso"));
                   a.setSemestre(rs2.getString("semestre"));
                   p = a;
                }if(rs.getString("tipo").equals("professor")){
                    String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, professor_oo.disciplina, professor_oo.salario"
                           + " FROM PessoaOO"
                           + "INNER JOIN professor_oo on pessoaoo3.id = professor_oo.id"
                           + "where professor_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                    
                    Professor prof = new Professor();
                    prof.setDisciplina(rs2.getString("disciplina"));
                    prof.setSalario(rs2.getDouble("salario"));
                    p = prof;
                    
                }if(rs.getString("tipo").equals("FuncAdm")){
                    String selectSQL2 = "SELECT pessoaoo3.id, pessoaoo3.nome, pessoaoo3.idade, pessoaoo3.endereco, funcadm_oo.funcao, funcadm_oo.setor, funcadm_oo.salario"
                           + " FROM PessoaOO"
                           + "INNER JOIN funcadm_oo on pessoaoo3.id = funcadm_oo.id"
                           + "where aluno_oo.id = ?";
                   //idzin
                   preparedStatement = dbConnection.prepareStatement(selectSQL2);
                   preparedStatement.setInt(1, rs.getInt("id"));
                   preparedStatement.executeUpdate();
                   rs2 = preparedStatement.getResultSet();
                    
                    FuncAdm fa = new FuncAdm();
                    fa.setFuncao(rs2.getString("funcao"));
                    fa.setSetor(rs2.getString("setor"));
                    fa.setSalario(rs2.getDouble("salario"));
                    p = fa;
                    
                }
          
                p.setId(rs.getInt("id"));
                p.setNome(rs.getString("nome"));
                p.setIdade(rs.getInt("idade"));
                p.setEndereco(rs.getString("endereco"));
                p.setTipo(rs.getString("tipo"));
                
                lista.add(p);
                
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
*/
}
