/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario{
    private String disciplina;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
    
    public Professor(String nome, int idade, String endereco, double salario, String disciplina){
        this.setNome(nome);
        this.setIdade(idade);
        this.setEndereco(endereco);
        this.setSalario(salario);
        this.disciplina = disciplina;  
    }
    
    public Professor(){ 
    }
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;
        
        String insertTableSQL = "INSERT INTO Pessoaoo3(id, nome, idade, endereco, tipo) VALUES( id_pessoa.nextval, ?, ?, ?, ?)";
        String insertTableSQL2 = "INSERT INTO Professor_oo(id, salario, disciplina) VALUES( id_pessoa.currval, ?, ?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
           
            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getTipo());    
            
            preparedStatement.executeUpdate();
            
            preparedStatement2 = dbConnection.prepareStatement(insertTableSQL2);
            
            preparedStatement2.setDouble(1, this.getSalario());
            preparedStatement2.setString(2, this.disciplina);
                          
            preparedStatement2.executeUpdate();
            System.out.println("Record is inserted into PessoaOO table!");
        }catch(SQLException e){
            e.printStackTrace();
        }       
    }
    
    public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        PreparedStatement ps2;
        
        String updateTableSQL = "UPDATE PessoaOO3 set nome = ?, idade = ?, endereco = ? where id = ?";
        String updateTableSQL2 = "UPDATE professor_oo set salario = ?, disciplina = ? where id = ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setString(1, this.getNome());
            ps.setInt(2, this.getIdade());
            ps.setString(3, this.getEndereco());
            ps.setInt(4, this.getId());
            ps.executeUpdate();
            
            ps2 = dbConnection.prepareStatement(updateTableSQL2);
            
            ps2.setDouble(1, this.getSalario());
            ps2.setString(2, this.disciplina);
            ps2.setInt(3, this.getId());            
            ps2.executeUpdate();
            
            System.out.println("Pessoa alterada para " + this.getNome() + "!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
}
