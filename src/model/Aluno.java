/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa{
    private String semestre;
    private String curso;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
    public Aluno(String nome, int idade, String endereco, String semestre, String curso){
        this.setNome(nome);
        this.setIdade(idade);
        this.setEndereco(endereco);
        this.semestre = semestre;
        this.curso = curso;        
    }
    
    public Aluno(){ 
    }
    
    public void inserir(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement2 = null;
        
        
        String insertTableSQL = "INSERT INTO Pessoaoo3(id, nome, idade, endereco, tipo) VALUES( id_pessoa.nextval, ?, ?, ?, ?)";
        String insertTableSQL2 = "INSERT INTO Aluno_oo(id, semestre, curso) VALUES( id_pessoa.currval, ?, ?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
           
            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getTipo());
            
            preparedStatement.executeUpdate();
            
            preparedStatement2 = dbConnection.prepareStatement(insertTableSQL2);
            
            preparedStatement2.setString(1, this.semestre);
            preparedStatement2.setString(2, this.curso);
                        
            preparedStatement2.executeUpdate();
            System.out.println("Record is inserted into PessoaOO table!");
        }catch(SQLException e){
            e.printStackTrace();
        }       
    }
    
    public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        PreparedStatement ps2;
        
        String updateTableSQL = "UPDATE PessoaOO3 set nome = ?, idade = ?, endereco = ? where id = ?";
        String updateTableSQL2 = "UPDATE aluno_oo set semestre = ?, curso = ? where id = ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setString(1, this.getNome());
            ps.setInt(2, this.getIdade());
            ps.setString(3, this.getEndereco());
            ps.setInt(4, this.getId());
            ps.executeUpdate();
            
            ps2 = dbConnection.prepareStatement(updateTableSQL2);
            
            ps2.setString(1, this.semestre);
            ps2.setString(2, this.curso);
            ps2.setInt(3, this.getId());
            ps2.executeUpdate();
            
            System.out.println("Pessoa alterada para " + this.getNome() + "!");
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
}
